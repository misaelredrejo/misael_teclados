<?php
include 'templates/header.php';
require_once(dirname(__FILE__) . '/app/controllers/indexController.php');
$teclados = indexAction();
?>
<!-- Bootstrap core CSS
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<link rel="stylesheet" href=".\assets\css\bootstrap.css">
<link rel="stylesheet" href=".\assets\css\style.css">
<div class="jumbotron jumbotron-fluid">
  <div id="bienvenida" class="container">
    <h1 class='index-title'>Bienvenid@ a Teclados Misael</h1>
    <?php
    if ($loggedin) echo "<span class='badge badge-default'> Has iniciado sesión: ".$user."</span>";
    else           echo "<span class='badge badge-default'> por favor, regístrate o inicia sesión.</span>";
    ?>
  </div>
</div>
<div class="contenedor">
  <div class="subcontenedor w-50 mx-auto">
    <h1>Catálogo de teclados</h1>
  </div>
  <div class="subcontenedor">
    <div id="carouselExampleControls" class="carousel slide w-50 mx-auto" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="assets/img/teclados01.jpg" class="d-block w-100 rounded" alt="carousel1">
        </div>
        <div class="carousel-item">
          <img src="assets/img/teclados06.jpg" class="d-block w-100 rounded" alt="carousel2">
        </div>
        <div class="carousel-item">
          <img src="assets/img/teclados07.jpg" class="d-block w-100 rounded" alt="carousel3">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <div class="subcontenedor w-75 mx-auto">
    <!-- Content Row -->
    <?php for ($i = 0; $i < sizeof($teclados); $i+=3) { ?>
    <!--   <div class="card-group">   -->
    <div class="row">
      <?php
      for ($j = $i; $j < ($i + 3); $j++) {
      if (isset($teclados[$j])) {
      
      echo $teclados[$j]->teclado2HTML();
      }
      }
      ?>
    </div>
    <!-- /.row -->
    <?php } ?>
  </div>
</div>
<!-- Footer -->
<footer class="page-footer pt-4">
  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">
    <!-- Grid row -->
    <div class="row">
      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h5 class="text-uppercase">Teclados Misael</h5>
        <p>Este proyecto está siendo desarrollado por Misael Redrejo.</p>
        <p>La página tratará de la venta de teclados mecánicos.</p>
        <p class="mt-auto">4Vientos 2ºDAM</p>
      </div>
      <!-- Grid column -->
      <hr class="clearfix w-100 d-md-none pb-3">
      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- Links -->
        <h5 class="text-uppercase">Links</h5>
        <ul class="list-unstyled">
          <li>
            <a href="http://www.cuatrovientos.org/" target="_blank">Cuatrovientos</a>
          </li>
          <li>
            <a href="https://www.instagram.com/cipcuatrovientos/?hl=es" target="_blank">Twitter</a>
          </li>
          <li>
            <a href="https://twitter.com/_cuatrovientos" target="_blank">Instagram</a>
          </li>
          <li>
            <a href="https://www.facebook.com/cipcuatrovientos/" target="_blank">Facebook</a>
          </li>
        </ul>
      </div>
      <!-- Grid column -->
      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- Links -->
        <h5 class="text-uppercase">Contacto</h5>
        <ul class="list-unstyled">
          <li>
            Tel: (34) 948 12 41 29
          </li>
          <li>
            info@cuatrovientos.org
          </li>
          <li>
            misaelredrejo@gmail.com
          </li>
        </ul>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row -->
  </div>
  <!-- Footer Links -->
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="https://mdbootstrap.com/"> MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
<!-- Bootstrap core JavaScript
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src=".\assets\js\bootstrap.js"></script>
</body>
</html>