<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/TecladoDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Teclado.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $name = ValidationsRules::test_input($_POST["name"]);
    $description = ValidationsRules::test_input($_POST["description"]);
    $urlPicture = ValidationsRules::test_input($_POST["urlPicture"]);
    $price = ValidationsRules::test_input($_POST["price"]);
    // TODOD hacer uso de los valores validados 
    $teclado = new Teclado();
    $teclado->setName($_POST["name"]);
    $teclado->setDescription($_POST["description"]);
    $teclado->setUrlPicture($_POST["urlPicture"]);
    $teclado->setPrice($_POST["price"]);

    //Creamos un objeto CreatureDAO para hacer las llamadas a la BD
    $tecladoDAO = new TecladoDAO();
    $tecladoDAO->insert($teclado);
    
    header('Location: ../../index.php');
    
}
?>

