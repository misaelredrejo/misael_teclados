<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/TecladoDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Teclado.php');

$tecladoDAO = new TecladoDAO();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
//Llamo que hace la edición contra BD
    deleteAction();
}

function deleteAction() {
    $id = $_GET["id"];

    $tecladoDAO = new TecladoDAO();
    $tecladoDAO->delete($id);

    header('Location: ../../index.php');
}
?>

