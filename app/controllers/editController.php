<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/TecladoDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Teclado.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $name = $_POST["name"];
    $urlPicture = $_POST["urlPicture"];
    $description = $_POST["description"];
    $price = $_POST["price"];

    $teclado = new Teclado();
    $teclado->setIdTeclado($id);
    $teclado->setName($name);
    $teclado->setUrlPicture($urlPicture);
    $teclado->setDescription($description);
    $teclado->setPrice($price);

    $tecladoDAO = new TecladoDAO();
    $tecladoDAO->update($teclado);

    header('Location: ../../index.php');
}

?>

