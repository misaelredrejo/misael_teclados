<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/TecladoDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Teclado.php');


function indexAction() {
    $TecladoDAO = new TecladoDAO();
    return $TecladoDAO->selectAll();
}

?>