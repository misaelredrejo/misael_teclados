<?php

class Teclado {

    private $idTeclado;
    private $name;
    private $description;
    private $urlPicture;
    private $price;

    public function __construct() {
        
    }

    public function getIdTeclado() {
        return $this->idTeclado;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public  function getUrlPicture() {
        return $this->urlPicture;
    }
    
    public function getPrice() {
        return $this->price;
    }

    public function setIdTeclado($idTeclado) {
        $this->idTeclado = $idTeclado;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setUrlPicture($urlPicture) {
        $this->urlPicture = $urlPicture;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    function setPrice($price) {
        $this->price = $price;
    }
    

//Función para pintar cada teclado
    function teclado2HTML() {
        
        $result = '<div class="col-sm-4 col-lg-4 col-md-4">';
            $result .= '<div class="card">';
                $result .= '<img class="my-img-size card-img-top rounded mx-auto d-block description" src="'.$this->getUrlPicture().'" alt="img">';
                $result .= '<div class="card-body">';
                    $result .= '<h4 class="d-flex justify-content-end price">'.$this->getPrice().' €</h4>';
                    $result .= '<h4 class="card-title"><a href="app/views/edit.php?id='.$this->getIdTeclado().'">' . $this->getName() . ':</a>';
                    $result .= '</h4>';
                    $result .= '<p class="card-text description">'.$this->getDescription().'</p>';
                    $result .= '<a type="button" class="btn btn-primary" href="app/views/detail.php?id='.$this->getIdTeclado().'">Ver más...</a>';
                    $result .= '<a type="button" class="btn btn-danger" href="app/controllers/deleteController.php?id='.$this->getIdTeclado().'">Borrar</a>';
                $result .= '</div>';
            $result .= '</div>';
        $result .= '</div>';
        
        
        return $result;
    }
    
    
}
