<?php

/* Clase UserDAO.php
 * Clase que aplica el Patrón de Diseño DAO para manejar toda la información de un objeto Usuario.
 * author Eugenia Pérez
 * mailto eugenia_perez@cuatrovientos.org
 */
require_once(dirname(__FILE__) . '/../../persistence/conf/PersistentManager.php');

class TecladoDAO {

    //Se define una constante con el nombre de la tabla
    const TECLADO_TABLE = 'teclado';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }
    
    public function selectAll() {
        $query = "SELECT * FROM " . TecladoDAO::TECLADO_TABLE;
        $result = mysqli_query($this->conn, $query);
        $teclados = array();
        while ($tecladoBD = mysqli_fetch_array($result)) {
            
            $teclado = new Teclado();
            $teclado->setIdTeclado($tecladoBD["idTeclado"]);
            $teclado->setName($tecladoBD["name"]);
            $teclado->setDescription($tecladoBD["description"]);
            $teclado->setUrlPicture($tecladoBD["urlPicture"]);
            $teclado->setPrice($tecladoBD["price"]);
            
            
            array_push($teclados, $teclado);
        }
        return $teclados;
    }

    /* Función de login de usuario */

    public function login($username, $password) {
        $query = "SELECT * FROM " . TecladoDAO::USER_TABLE .
                " WHERE username = '" . $username . "' AND password='" .
                $password . "'";
        $result = mysqli_query($this->conn, $query);

        if ($result && mysqli_num_rows($result) > 0) {
            $userBD = mysqli_fetch_array($result);
            $user = new User();
            $user->set_ID($userBD['iduser']);
            $user->set_username($userBD['username']);
            $user->set_password($userBD['password']);

            return $user;
        } 
        return null;
    }

    /*
     * Inserta un usuario en la base de datos. Como la clave primaria es 
     * autogenerada, no es necesario insertarla.
     * Es importante que a los valores a insertar que pongas dentro del 
     * paréntesis VALUES, los encierres en comillas simples.
     */

    public function insert($teclado) {
        $query = "INSERT INTO " . TecladoDAO::TECLADO_TABLE .
                " (name, description, urlPicture, price) VALUES(?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $teclado->getName();
        $description = $teclado->getDescription();
        $urlPicture = $teclado->getUrlPicture();
        $price = $teclado->getPrice();
        
        mysqli_stmt_bind_param($stmt, 'sssi', $name, $description, $urlPicture, $price);
        return $stmt->execute();
    }
    
    public function selectById($id) {
        $query = "SELECT name, description, urlPicture, price FROM " . TecladoDAO::TECLADO_TABLE . " WHERE idTeclado=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $description, $urlPicture, $price);

        $teclado = new Teclado();
        while (mysqli_stmt_fetch($stmt)) {
            $teclado->setIdTeclado($id);
            $teclado->setName($name);
            $teclado->setDescription($description);
            $teclado->setUrlPicture($urlPicture);
            $teclado->setPrice($price);
       }

        return $teclado;
    }
    
    public function update($teclado) {
        $query = "UPDATE " . TecladoDAO::TECLADO_TABLE .
                " SET name=?, description=?, urlPicture=?, price=?"
                . " WHERE idTeclado=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $teclado->getName();
        $description= $teclado->getDescription();
        $urlPicture = $teclado->getUrlPicture();
        $price = $teclado->getPrice();
        $id = $teclado->getIdTeclado();
        mysqli_stmt_bind_param($stmt, 'sssii', $name, $description, $urlPicture,$price ,$id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . TecladoDAO::TECLADO_TABLE . " WHERE idTeclado=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

}

?>
