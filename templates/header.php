<?php

  // Obtenemos el directorio del proyecto para establecer rutas relativas.
  $dir = __DIR__;

  require_once $dir . '/../utils/SessionHelper.php';

  /// =================================
  /// Gestión de la sesión de usuario:
  /// =================================

  // String para almacenar el nombre de usuario, por defecto "Invitado"
  $userstr = '(Invitado)';
  $appname = 'Teclados Misael';
  // TODO Almacena en la variable $loggedin el valor retornado de la función loggedin de SessionHelper
  $loggedin = SessionHelper::loggedIn();

  if ($loggedin) {
    $user = $_SESSION['user'];
    $userstr = " ($user)";
  }

?>
<head>
    <meta charset="utf-8">
    <title><?php echo "$appname $userstr" ?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href=".\..\assets\css\bootstrap.css">
    <link rel="icon" type="image/png" href="assets/img/iconTeclado.png" />

</head>

<body>
<?php
  // En caso de tener una sesión registrada con antelación mostramos las opciones avanzadas de la aplicación
  if ($loggedin)
  {
  ?>
    <nav class="navbar my-navbar navbar-expand-lg">
        <button class="navbar-toggler navbar-toggler-right" type="button"
                data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarToggler" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="./index.php"><img src="assets/img/iconTeclado.png" width="30px" alt="teclado">Teclados Misael</a>

        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav navbar-list mr-auto mt-2 mt-md-0">
                <li class="nav-item navbar-list-item">
                    <a class="nav-link" href="app/logout.php">Salir</a>
                </li>
                <li class="nav-item navbar-list-item">
                    <a class="nav-link" href="app/views/insert.php">Crear teclado</a>
                </li>
            </ul>
        </div>
    </nav>
  <?php
}
else {
  // En caso de ser usuario no registrado, (Invitado)
  ?>
    <nav class="navbar my-navbar navbar-expand-lg">
        <button class="navbar-toggler navbar-toggler-right" type="button"
                data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarToggler" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="./index.php"><img src="assets/img/iconTeclado.png" width="30px" alt="teclado">Teclados Misael</a>

        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav navbar-list mr-auto mt-2 mt-md-0">
                <li class="nav-item navbar-list-item">
                    <a class="nav-link " href="app/signup.php">Registrarse</a>
                </li>
                <li class="nav-item navbar-list-item">
                    <a class="nav-link" href="app/login.php">Entrar</a>
                </li>
                <li class="nav-item navbar-list-item">
                    <a class="nav-link" href="app/logout.php">Salir</a>
                </li>
                <li class="nav-item navbar-list-item">
                    <a class="nav-link" href="app/views/insert.php">Crear teclado</a>
                </li>
            </ul>
        </div>
    </nav>
  <?php
}
?>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src=".\..\assets\js\bootstrap.js"></script>
